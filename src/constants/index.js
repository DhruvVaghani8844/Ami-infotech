import {
  NFT,
  token,
  Appdev,
  blockchain2,
  webde,
  webdeg,
  ux,
  Dapp,
  mobile,
  backend,
  creator,
  web,
  javascript,
  typescript,
  html,
  css,
  reactjs,
  redux,
  tailwind,
  nodejs,
  mongodb,
  git,
  figma,
  docker,
  meta,
  starbucks,
  tesla,
  shopify,
  carrent,
  jobit,
  tripguide,
  threejs,
} from "../assets";
import imagee from "../assets/1.jpg"
export const navLinks = [
  {
    id: "about",
    title: "About",
  },
  {
    id: "work",
    title: "Work",
  },
  {
    id: "contact",
    title: "Contact",
  },
];

const services = [
  {
    title: "Smart Contracts Development",
    icon: web,
  },
  {
    title: "Blockchain development",
    icon: mobile,
  },
  {
    title: "NFT projects",
    icon: backend,
  },
  {
    title: " DApp Creation",
    icon: creator,
  },
  {
    title: "Tokenization",
    icon: creator,
  },
];

const technologies = [
  {
    name: "HTML 5",
    icon: html,
  },
  {
    name: "CSS 3",
    icon: css,
  },
  {
    name: "JavaScript",
    icon: javascript,
  },
  {
    name: "React JS",
    icon: reactjs,
  },
  {
    name: "Tailwind CSS",
    icon: tailwind,
  },
  {
    name: "Node JS",
    icon: nodejs,
  },
  {
    name: "MongoDB",
    icon: mongodb,
  },
];

const experiences = [
  {
    title: "Discover",
    // company_name: "Starbucks",
    // icon: starbucks,
    iconBg: "#383E56",
    // date: "March 2020 - April 2021",
    points: [
      "Analysis & Documentation, the important steps we adapt to built something awesome, so that you get satisfactory results.",
    ],
  },
  {
    title: "Design",
    // company_name: "Tesla",
    // icon: tesla,
    iconBg: "#383E56",
    // date: "Jan 2021 - Feb 2022",
    points: [
      "Through UI we create user-friendly interfaces that enable users to understand how to use complex technical products..",
    ],
  },
  {
    title: "Build",
    // company_name: "Shopify",
    // icon: shopify,
    iconBg: "#383E56",
    // date: "Jan 2022 - Jan 2023",
    points: [
      "We provide solutions with innovative ideas. Our objective is to use the latest technologies and platforms.",
    ],
  },
  {
    title: "Deliver",
    // company_name: "Meta",
    // icon: meta,
    iconBg: "#383E56",
    // date: "Jan 2023 - Present",
    points: [
      "We always make sure that the end result that we deliver is able to satisfy your needs and requirements.",
    ],
  },
];

const testimonials = [
  {
    testimonial:
      "We are revolutionising your curent business into the blockchain base platform. We are offering cutting-edge solution more efficient and faster way into your business model..",
    // name: "Sara Lee",
    // designation: "CFO",
    // company: "Acme Co",
    // image: "https://randomuser.me/api/portraits/women/4.jpg",
  },
  {
    testimonial:
    // "
      "Our process is simply designed to make your journey between idea and implementation frictionless and infallible.",
    // name: "Chris Brown",
    // designation: "COO",
    // company: "DEF Corp",
    // image: "https",
  },
  {
    testimonial:
      "We have been offering top-notch services to clients belonging to different industries for a long time. Moreover, we believe in developing long-term relationships with our clients.",
    // name: "Lisa Wang",
    // designation: "CTO",
    // company: "456 Enterprises",
    // image: "https://randomuser.me/api/portraits/women/6.jpg",
  },
];

const projects = [
  {
    name: "Blockchain",
    description:
      "For whatever purpose, we can build a private or custom blockchain for you. Private blockchains are permission-based networks with only known partners as members.",
    tags: [
      // {
      //   name: "react",
      //   color: "blue-text-gradient",
      // },
      // {
      //   name: "mongodb",
      //   color: "green-text-gradient",
      // },
      // {
      //   name: "tailwind",
      //   color: "pink-text-gradient",
      // },
    ],
    image: blockchain2,
    // source_code_link: "https://github.com/",
  },
  {
    name: "DApp Creation",
    description:
      "Decentralized applications are digital applications or programs that exist and run on a blockchain or peer-to-peer network of computers instead of a single computer.",
    tags: [
     
    ],
    image: Dapp,
    // source_code_link: "https://github.com/",
  },
  {
    name: "NFT projects",
    description:
      "Develop with us to gain from our experience of working with several projects and avoid mistakes that can hamper your business.",
    tags: [
      // {
      //   name: "nextjs",
      //   color: "blue-text-gradient",
      // },
      // {
      //   name: "supabase",
      //   color: "green-text-gradient",
      // },
      // {
      //   name: "css",
      //   color: "pink-text-gradient",
      // },
    ],
    image: NFT,
    // source_code_link: "https://github.com/",
  },
  {
    name: "Token creation",
    description:
      "Our experts create token for you and we ensure that our ICO services are customized to your preferences to best suit your business operations.",
    tags: [
      // {
      //   name: "nextjs",
      //   color: "blue-text-gradient",
      // },
      // {
      //   name: "supabase",
      //   color: "green-text-gradient",
      // },
      // {
      //   name: "css",
      //   color: "pink-text-gradient",
      // },
    ],
    image: token,
    
  },
  {
    name: "Web development",
    description:
      "create your website and expose your work World Wide..",
    tags: [
      // {
      //   name: "nextjs",
      //   color: "blue-text-gradient",
      // },
      // {
      //   name: "supabase",
      //   color: "green-text-gradient",
      // },
      // {
      //   name: "css",
      //   color: "pink-text-gradient",
      // },
    ],
    image: webde,
    // source_code_link: "https://github.com/",
  },
  {
    name: "App development",
    description:
      "To create an application to be used on smartphones, tablets and other mobile devices, and grow your business",
    tags: [
      // {
      //   name: "nextjs",
      //   color: "blue-text-gradient",
      // },
      // {
      //   name: "supabase",
      //   color: "green-text-gradient",
      // },
      // {
      //   name: "css",
      //   color: "pink-text-gradient",
      // },
    ],
    image: Appdev,
    // source_code_link: "https://github.com/",
  },
  {
    name: "Web design",
    description:
      "the design of websites that are displayed on the internet, helps to capture user intention and spend more time on website.",
    tags: [
      // {
      //   name: "nextjs",
      //   color: "blue-text-gradient",
      // },
      // {
      //   name: "supabase",
      //   color: "green-text-gradient",
      // },
      // {
      //   name: "css",
      //   color: "pink-text-gradient",
      // },
    ],
    image: webdeg,
    // source_code_link: "https://github.com/",
  },
  {
    name: "UI/UX Design",
    description:
      "create user-friendly interfaces that enable users to understand how to use complex technical products.",
    tags: [
      // {
      //   name: "nextjs",
      //   color: "blue-text-gradient",
      // },
      // {
      //   name: "supabase",
      //   color: "green-text-gradient",
      // },
      // {
      //   name: "css",
      //   color: "pink-text-gradient",
      // },
    ],
    image: ux,
    // source_code_link: "https://github.com/",
  },
];

export { services, technologies, experiences, testimonials, projects };
